'use strict'
var Endpoint = require('./Endpoint'),
	cheerio = require('cheerio'),
	Error = require('../Responses/Error'),
	errorMessages = require('../Responses/error_messages.json'),
	responseCodes = require('../Responses/response_codes.json')

module.exports = class MatchGraphsEndpoint extends Endpoint {
	Path() {
		return '/summoner/matches/ajax/detail/gold/'
	}

	Parse($, json) {
		var data = {},
			team = {},
			player = {}

		var script = $("script").html();
		
		var gold = JSON.parse(script.split('titleAxisY: \'Total Gold\',\n\t\t\tseries: ')[1].split(',\n\t\t\tcategories:')[0]);
		var exp = JSON.parse(script.split('titleAxisY: \'Total Exp\',\n\t\t\tseries: ')[1].split(',\n\t\t\tcategories:')[0]);
		var cs = JSON.parse(script.split('titleAxisY: \'Total CS\',\n\t\t\tseries: ')[1].split(',\n\t\t\tcategories:')[0]);
		for (var i = 0; i < 10; i++) {
			var name = this.getItemID(gold[i].name);
			gold[i].name = name;
			exp[i].name = name;
			cs[i].name = name;
			delete gold[i].color;   delete exp[i].color;   delete cs[i].color;
			delete gold[i].visible; delete exp[i].visible; delete cs[i].visible;
		}

		data.gold = gold;
		data.exp = exp;
		data.cs = cs;
		data.time = [...Array(gold[0].data.length).keys()];

		return data
	}

	getItemID(str) {
		if (!str || typeof str !== 'string' || !str.length) return str
		var regex = new RegExp(/\/([0-9A-Za-z]*)\.png/g).exec(str)
		return !!regex && regex.length ? regex[1] : str
	}
}